***8.cir***

Vs 1 0 pulse(0 5 0 0 0 1 2)
Vi 1 2 0
R 2 3 1k
C 3 0 1u
.tran 1u 5m 0m
.options hcopydevtype=postscript
.probe
.end


ngspice 71 -> source 8cir.txt
ngspice 72 -> run
ngspice 76 -> plot i(Vi) xlabel 't' ylabel 'E[V],i[A]'
ngspice 77 -> hardcopy 8cir.ps  i(Vi) xlabel 't' ylabel 'E[V],i[A]'
The file "8cir.ps" may be printed on a postscript printer.
ngspice 78 -> 
