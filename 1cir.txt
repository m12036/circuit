***1.cir***

Vs 1 0
R1 1 2 2
Vi 2 0 0
.dc Vs 0 8 0.1
.options hcopydevtype=postscript
.probe
.end

ngspice 3 -> source 1cir.txt
ngspice 4 -> run
ngspice 13 -> plot i(Vi) xlabel 'E[V]' ylabel 'i[A]'
ngspice 15 -> hardcopy 1cir.ps i(Vi) xlabel 'E[V]' ylabel 'i[A]'
The file "1cir.ps" may be printed on a postscript printer.

