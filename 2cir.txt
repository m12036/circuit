***2.cir***

Vs 1 0 24
Vi 1 2 0
R1 2 3 3
R2 3 4 4
R3 4 0 5
.op
.probe
.end


ngspice 33 -> print i(Vi)
i(vi) = 2.000000e+00
ngspice 34 -> print v(2,3)
v(2,3) = 6.000000e+00
ngspice 35 -> print v(3,4)
v(3,4) = 8.000000e+00
ngspice 36 -> print v(4)
v(4) = 1.000000e+01
