***5.cir***

Vs 1 0 sin(0 1 50)
R 1 2 2
Vi 2 0 0
.tran 10u 40m 20m
.options hcopydevtype=postscript
.probe
.end

ngspice 46 -> source 5cir.txt
ngspice 47 -> run
ngspice 53 -> hardcopy 5cir.ps  v(1),i(Vi) xlabel 't' ylabel 'E[V],i[A]'
ngspice 54 -> hardcopy 5cir.ps  v(1),i(Vi) xlabel 't' ylabel 'E[V],i[A]'
The file "5cir.ps" may be printed on a postscript printer.
